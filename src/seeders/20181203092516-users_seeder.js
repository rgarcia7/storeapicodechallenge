'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Users', [{
        user_username:"admin",user_name:"Admin",user_password:"password",user_role:1},
        {user_username:"wperryman0",user_name:"Walt Perryman",user_password:"password",user_role:0},
        {user_username:"ckuhne1",user_name:"Cad Kuhne",user_password:"password",user_role:1},
        {user_username:"ggoggin2",user_name:"Garald Goggin",user_password:"password",user_role:1},
        {user_username:"talgar3",user_name:"Tabbie Algar",user_password:"password",user_role:1},
        {user_username:"acuckson4",user_name:"Angelico Cuckson",user_password:"password",user_role:0},
        {user_username:"jdegg5",user_name:"Janeczka Degg",user_password:"password",user_role:1},
        {user_username:"fberthomier6",user_name:"Fee Berthomier",user_password:"password",user_role:1},
        {user_username:"ssmalls7",user_name:"Shirlee Smalls",user_password:"password",user_role:1},
        {user_username:"wbenwell8",user_name:"Woodrow Benwell",user_password:"password",user_role:1},
        {user_username:"fgoble9",user_name:"Farleigh Goble",user_password:"password",user_role:1
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Users', null, {});
  }
};
