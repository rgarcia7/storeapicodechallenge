'use strict';
module.exports = (sequelize, DataTypes) => {
  const PriceUpdates = sequelize.define('PriceUpdates', {
    product_id: DataTypes.INTEGER,
    product_newPrice: DataTypes.DECIMAL,
    product_oldPrice: DataTypes.DECIMAL
  }, {});
  PriceUpdates.associate = function(models) {
    // associations can be defined here
  };
  return PriceUpdates;
};