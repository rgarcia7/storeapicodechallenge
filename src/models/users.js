'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    user_username: DataTypes.STRING,
    user_name: DataTypes.STRING,
    user_password: DataTypes.STRING,
    user_role: DataTypes.SMALLINT
  }, {});
  Users.associate = function(models) {
    // associations can be defined here
  };
  return Users;
};