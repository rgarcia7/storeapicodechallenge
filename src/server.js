var restify = require('restify'); // require the restify library.
var paginate = require('restify-paginate'); 
var models = require('./models/index');//require models
var productController = require('./controllers/productController'); //require product controller
var userController = require('./controllers/userController');//require user controller
var adminController = require('./controllers/admin/adminController');//require user controller


var server = restify.createServer(); // create an HTTP server.

var paginateOptions = {
    paramsNames: {
        page: 'page',           // Page number param name
        per_page: 'per_page'    // Page size param name
    },
    defaults: {                 // Default values
        page: 1,
        per_page: 10
    },
    numbersOnly: false,         // Generates the full links or not
    hostname: true              // Adds the hostname in the links or not
};
server.use(restify.queryParser());//Parses URL query parameters into req.query, additionally params are merged into req.params
server.use(restify.bodyParser());//Parses post body paramters into req.body
server.use(restify.authorizationParser());//Parses out the Authorization header,. Currently only HTTP Basic Auth and HTTP Signature schemes are supported.

server.use(paginate(server,paginateOptions));//Initialize the paginate module

productController.use(server,models);//Initialize the product controller
userController.use(server,models);//Initialize the user controller
adminController.use(server,models);//Initialize the user controller

server.listen(process.env.PORT || 80, function () { // bind server to port 8080.
  console.log('%s listening at %s', server.name, server.url);
});