var MacaroonsBuilder = require('macaroons.js').MacaroonsBuilder;
var MacaroonsVerifier = require('macaroons.js').MacaroonsVerifier;

module.exports.checkUser =function(models,req, res, next) {
        //Gets data from header parameters
		console.log(req.header('Token'));
		if(req.header('Token') != null){
			return this.checkLogin(models,req,res,next);
		}else{
			var data = {
				status: "No Token prodvided",
				data : null
			};
			res.send(data);//Promises being promises
			next();
		}
}

module.exports.loginUser =function(models,req, res, next) {
        //Gets data from header parameters
		console.log(req.header('Token'));
		if(req.header('Token') != null){
			return this.checkLogin(models,req,res,next);
		}else{
			//Gets data from body parameters
			models.Users.findOne({ where: {user_username: req.authorization.basic.username, user_password: req.authorization.basic.password} }).then(function(user) {
			  	if(user==null){
				  	var data = {
						status: "userNotFound",
						data : user
					};
					res.send(data);//Promises being promises
					next();
				}else{
					var adminTokenG=null;
					if(user.user_role == 1){
						var location = "restApiChallenge.org";//dummy location
						var secretKey = "Admin this is our super ultra mega secret key; only we should know it";
						var identifier = "Your login is valid";
						var macaroon =new MacaroonsBuilder(location, secretKey, identifier)
										.add_first_party_caveat("user_username = " + req.authorization.basic.username)
									    .add_first_party_caveat("user_role = " + user.user_role)
									    .getMacaroon();
						adminTokenG =macaroon.serialize();
					}
					var location = "restApiChallenge.org";//dummy location
					var secretKey = "this is our super ultra mega secret key; only we should know it";
					var identifier = "Your login is valid";
					var macaroon =new MacaroonsBuilder(location, secretKey, identifier)
								    .add_first_party_caveat("user_username = " + req.authorization.basic.username)
								    .getMacaroon();
					var data = {
						status: "userFound",
						token: macaroon.serialize(),
						aToken:adminTokenG
					};
					res.send(data);
					next();
				}		
			});
		}
}

module.exports.checkLogin = function(models,req, res, next) {
        var macaroon = MacaroonsBuilder.deserialize(req.header('Token'));
		var verifier = new MacaroonsVerifier(macaroon);
		verifier.satisfyExact("user_username = " + req.header('username'));
		var secretKey = "this is our super ultra mega secret key; only we should know it";
		if(verifier.isValid(secretKey)){
			var data = {
				status: "userFound And logged"
			};
			return data;
		}else{
			var data = {
				status: "userNotFound"
			};
			res.send(data);
			next();
		}
}
