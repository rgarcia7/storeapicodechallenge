var adminAuth = require('./adminAuth');//require user controller

module.exports.use = function (server,models) {
	server.post('/manage/product/setprice',checkProductOnPrice);//?productid={{productid}}&price={{price}}
	server.post('/manage/product/setstock',checkProductOnStock);//?productid={{productid}}&stock={{price}}
	server.post('/manage/product/add',checkOnAddProduct);
	server.del('/manage/product/del',checkOnDelProduct);//?productid={{productid}}

	function checkOnDelProduct(req,res,next){ //Function that receives the request, response, and a next funtion
		if(req.query['productid'] == null || isNaN(parseFloat(req.query['productid'],10)) || !Number.isInteger(parseFloat(req.query['productid'],10)) ){//Check if tthe value is an integer
			var data = {
				status: "Error Not valid query check parameters"
			};
			res.send(data);
			next();
		}else{
			models.Product.findById(req.query['productid'])//Loads the product models and queries by id
				.then(function(product) {//Then executes coder after the query completion
					if(product==null){
						var data = {		//var data will be the json data to be sent on the response
							status: "Product Not Found",
							data : product
						};
						res.send(data);		//res.send() will send data to the client
						next();				//Calling next() in order to run the next handler in the chain
					}else{
						deleteProduct(req,res,next);
					}
			});
		}
	}

	function deleteProduct(req,res,next){
		var response =adminAuth.checkAdmin(models,req,res,next);//Validate User
		if(response && response.status){//Promises async
			models.Product.destroy({
		        where: {
		            id: req.query['productid']
		        }
		    }).then(function(product) {
		        var data = {
		            status: "Deleted product successfully"
		        };
		        res.send(data);
		        next();
		    });
		}
	}


	function checkOnAddProduct(req,res,next){
		models.Product.findOne({
			  where: {
			    product_name: req.body.productName
			  }
			})//Loads the product models and queries by id
			.then(function(product) {//Then executes coder after the query completion
				if(product==null){//No product exist with given name
					addProduct(req,res,next);
				}else{
					var data = {
						status: "Product with this name is already on stock",
						data : product
					};
					res.send(data);
				}
				next();
		});
	}

	function addProduct(req,res,next){ //Function that receives the request, response, and a next funtion
		var response =adminAuth.checkAdmin(models,req,res,next);//Validate User
		if(response && response.status){//Promises async
			models.Product.create({
			 	product_name:req.body.productName, 
				product_price: req.body.productPrice,
				product_quantity: req.body.productStock},
				{ fields: [ 
					'product_name',
					'product_price',
					'product_quantity'
					] 
				}
			).then(function(newProduct){
				var data = {
					status: "New Product Added",
					data: newProduct
				};
				res.send(data);
				next();
			});
		}	
	}

	function checkProductOnStock(req,res,next){ //Function that receives the request, response, and a next funtion
		if(req.query['stock']==null || req.query['productid'] == null || isNaN(parseFloat(req.query['stock'],10)) || !Number.isInteger(parseFloat(req.query['stock'],10)) ){//Check if tthe value is an integer
			var data = {
				status: "Error Not valid query check parameters"
			};
			res.send(data);
			next();
		}else{
			models.Product.findById(req.query['productid'])//Loads the product models and queries by id
				.then(function(product) {//Then executes coder after the query completion
					if(product==null){
						var data = {		//var data will be the json data to be sent on the response
							status: "Product Not Found",
							data : product
						};
						res.send(data);		//res.send() will send data to the client
						next();				//Calling next() in order to run the next handler in the chain
					}else{
						setStock(req,res,next,product.product_price);
					}
			});
		}
	}

	function checkProductOnPrice(req,res,next){ //Function that receives the request, response, and a next funtion
		if(req.query['price']==null ||req.query['productid'] == null || isNaN(req.query['price'])){//isNaN checks if the value is a valid number
			var data = {
				status: "Error Not valid query check parameters"
			};
			res.send(data);
			next();
		}else{
			models.Product.findById(req.query['productid'])//Loads the product models and queries by id
				.then(function(product) {//Then executes coder after the query completion
					if(product==null){
						var data = {		//var data will be the json data to be sent on the response
							status: "Product Not Found",
							data : product
						};
						res.send(data);		//res.send() will send data to the client
						next();				//Calling next() in order to run the next handler in the chain
					}else{
						setPrice(req,res,next,product.product_price);
					}
			});
		}
	}

	function setPrice(req,res,next,productPrice){
		var response =adminAuth.checkAdmin(models,req,res,next);//Validate User
		if(response && response.status){//Promises async
			models.PriceUpdates.create({ product_id:req.query['productid'], product_newPrice: req.query['price'], product_oldPrice: productPrice}, { fields: [ 'product_id', 'product_newPrice' , 'product_oldPrice' ] });
			models.Product.update({ product_price: req.query['price'] }, { where: {id:req.query['productid']}, returning: true}).then(function(price) {
				var data = {		//var data will be the json data to be sent on the response
					status: "Price Updated",
					data : price[1][0]//Access new value
				};
				res.send(data);
				next();
			});		
		}		
	}

	function setStock(req,res,next,productStock){
		var response =adminAuth.checkAdmin(models,req,res,next);//Validate User
		if(response && response.status){//Promises async
			models.Product.update({ product_quantity: req.query['stock'] }, { where: {id:req.query['productid']}, returning: true}).then(function(stock) {
				var data = {		//var data will be the json data to be sent on the response
					status: "Stock Updated",
					data : stock[1][0]//Access new value
				};
				res.send(data);
				next();
			});		
		}		
	}
};