var MacaroonsBuilder = require('macaroons.js').MacaroonsBuilder;
var MacaroonsVerifier = require('macaroons.js').MacaroonsVerifier;

module.exports.checkAdmin =function(models,req, res, next) {
        //Gets data from header parameters
		console.log(req.header('Token'));
		if(req.header('Token') != null && req.header('AToken') != null ){
			return this.checkAdminLogin(models,req,res,next);
		}else{
			var data = {
				status: "Token and AToken required",
				data :  null
			};
			res.send(data);//Promises being promises
			next();
		}
    }

module.exports.checkAdminLogin = function(models,req, res, next) {
        var macaroonUser = MacaroonsBuilder.deserialize(req.header('Token'));
		var verifierUser = new MacaroonsVerifier(macaroonUser);
		var secretUserKey = "this is our super ultra mega secret key; only we should know it";

		var macaroonAdmin = MacaroonsBuilder.deserialize(req.header('AToken'));
		var verifierAdmin = new MacaroonsVerifier(macaroonAdmin);
		var secretAdminKey = "Admin this is our super ultra mega secret key; only we should know it";
		verifierUser.satisfyExact("user_username = " + req.header('username'));
		verifierAdmin.satisfyExact("user_username = " + req.header('username'));
		verifierAdmin.satisfyExact("user_role = 1");
		if(verifierUser.isValid(secretUserKey)){
			if(verifierAdmin.isValid(secretAdminKey)){
				var data = {
					status: "AdminUser Found And logged"
				};
				return data;
			}else{
				var data = {
					status: "You're not an admin"
				};
				res.send(data);
				next();
			}
		}else{
			var data = {
				status: "userNotFound"
			};
			res.send(data);
			next();
		}
}
