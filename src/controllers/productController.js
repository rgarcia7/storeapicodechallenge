var userAuth = require('./userAuth');//require user controller
var Sequelize = require('sequelize');
module.exports.use = function (server,models) { 
	// add a route that listens on http://{{dockerMachineIp}}:8080/products/id/asc
	server.get('/products/id/asc',getAllProductsbyIDAsc);	/*  defines the function that is executed 
																when a user makes the request to the 
																URI /products/id/asc using the GET HTTP verb*/
	server.get('/products/id/des',getAllProductsbyIDDes);
	server.get('/products/name/asc',getAllProductsbyNameAsc);
	server.get('/products/name/des',getAllProductsbyNameDes);
	server.get('/products/likes/asc',getAllProductsbyLikeAsc);
	server.get('/products/likes/des',getAllProductsbyLikeDes);

	server.get('/product/:id',getProductbyID);
	server.put('/product/:id/:loggedUser/togglelike',checkProductExists);
	server.get('/products/search/:productname',searchProductByName);

	function searchProductByName(req,res,next){ //Function that receives the request, response, and a next funtion
		const Op = Sequelize.Op;//exposes symbol operators that can be used for to create more complex comparisons
		models.Product.findAll({
			  where: {
			    product_name: {[Op.like]: '%'+ req.params['productname'] + '%'}
			  }
			})
			.then(function(product) {//Then executes coder after the query completion
				console.log(product);
				if(product === undefined || product.length == 0){
					var data = {		//var data will be the json data to be sent on the response
						status: "Search did not return results for: " + req.params['productname'],
						data : product
					};
					res.send(data);		//res.send() will send data to the client
					next();	
				}else{
					var data = {		//var data will be the json data to be sent on the response
						status: "done",
						data : product
					};
					res.send(data);		//res.send() will send data to the client
					next();				//Calling next() in order to run the next handler in the chain
				}				
			});
	}


	function getProductbyID(req,res,next){ //Function that receives the request, response, and a next funtion
		models.Product.findById(req.params['id'])//Loads de product models and queries by id
			.then(function(product) {//Then executes coder after the query completion
				var data = {		//var data will be the json data to be sent on the response
					status: "done",
					data : product
				};
				res.send(data);		//res.send() will send data to the client
				next();				//Calling next() in order to run the next handler in the chain
			});
	}

	function checkProductExists(req,res,next){
		var response =userAuth.checkUser(models,req,res,next);//If the user has a token in headers
		if(response==null){
			var data = {
				status: "Error Not ValidaHeaders"//No token was provided
			};
			res.send(data);
			next();
		}
		if(response!=null && response.status!="userFound And logged"){//Validates if the user is using a valid token
			var data = {
				status: "Error Not Valid User"
			};
			res.send(data);
			next();
		}else{
			console.log("req.params");
			console.log(req.params);
			models.Product.findById(req.params['id'])
				.then(function(product) {
					if(product!=null){
						addlikeToProduct(req,res,next);
					}else{
						var data = {
							status: "error",
							data : product
						};
						res.send(data);
					}
					next();
				});

		}
	}

	function addlikeToProduct(req,res,next){ //This toggles likes on or off based on product and user
		models.Likes.find({where: { user_id: req.params['loggedUser'] }})
			.then(function(product) {
				var data;
				if(product!=null){//If this user has a like on current product:
					product.destroy();//delete the db row with the like transaction
					models.Product.decrement('product_likes', {where: { id: parseInt(req.params['id'], 10) }}); //Decrement likes on product where id = req.params - Casting to integer
					data = {
						status: "dislike",
						data : null
					};
					res.send(data);
					next();
				}else{//If this user doesnt has a like on current product:
					models.Product.increment('product_likes', {where: { id: parseInt(req.params['id'], 10) }}); //Increment likes on product where id = req.params - Casting to integer
					//create the row with the transaction
					models.Likes.create({ product_id: req.params['id'], user_id: req.params['loggedUser'] }, { fields: [ 'product_id', 'user_id' ] }).then(function(like) {
						data = {
							status: "like",
							data : like.get()
						};
						res.send(data);
						next();
					});
				}
			});
	}

	function getAllProductsbyIDAsc(req,res,next){
		models.Product.findAll({})
			.then(function(products) {
				var paginatedResponse = res.paginate.getPaginatedResponse(products);
				var data = {
					status: "done",
					data : paginatedResponse.data,
					pages: paginatedResponse.pages
				};
				res.send(data);
				next();
			});
	}

	function getAllProductsbyIDDes(req,res,next){
		models.Product.findAll({
			order: [
	            ['id', 'DESC']
	        ]
		})
			.then(function(products) {
				var paginatedResponse = res.paginate.getPaginatedResponse(products);
				var data = {
					status: "done",
					data : paginatedResponse.data,
					pages: paginatedResponse.pages
				};
				res.send(data);
				next();
			});
	}

	function getAllProductsbyNameAsc(req,res,next){
		models.Product.findAll({
			order: [
	            ['product_name', 'ASC']
	        ]
		})
			.then(function(products) {
				var paginatedResponse = res.paginate.getPaginatedResponse(products);
				var data = {
					status: "done",
					data : paginatedResponse.data,
					pages: paginatedResponse.pages
				};
				res.send(data);
				next();
			});
	}

	function getAllProductsbyNameDes(req,res,next){
		models.Product.findAll({
			order: [
	            ['product_name', 'DESC']
	        ]
		})
			.then(function(products) {
				var paginatedResponse = res.paginate.getPaginatedResponse(products);
				var data = {
					status: "done",
					data : paginatedResponse.data,
					pages: paginatedResponse.pages
				};
				res.send(data);
				next();
			});
	}

	function getAllProductsbyLikeAsc(req,res,next){
		models.Product.findAll({
			order: [
	            ['product_likes', 'ASC']
	        ]
		})
			.then(function(products) {
				var paginatedResponse = res.paginate.getPaginatedResponse(products);
				var data = {
					status: "done",
					data : paginatedResponse.data,
					pages: paginatedResponse.pages
				};
				res.send(data);
				next();
			});
	}

	function getAllProductsbyLikeDes(req,res,next){
		models.Product.findAll({
			order: [
	            ['product_likes', 'DESC']
	        ]
		})
			.then(function(products) {
				var paginatedResponse = res.paginate.getPaginatedResponse(products);
				var data = {
					status: "done",
					data : paginatedResponse.data,
					pages: paginatedResponse.pages
				};
				res.send(data);
				next();
			});
	}
};