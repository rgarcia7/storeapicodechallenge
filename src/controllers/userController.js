var userAuth = require('./userAuth');//require user controller
module.exports.use = function (server,models) {
	server.post('/api/login',loginUser);
	server.post('/product/buy',checkBuyProduct);//?buyQuantity={{quantity}}

	function checkBuyProduct(req,res,next){ //Function that receives the request, response, and a next funtion
		if(req.query['buyQuantity']==null || req.query['productid'] == null ){
			var data = {
				status: "Error Not valid query check parameters"
			};
			res.send(data);
			next();
		}else{
			var response =userAuth.checkUser(models,req,res,next);//If the user has a token in headers
			if(response==null){
				var data = {
					status: "Error Not ValidaHeaders"//No token was provided
				};
				res.send(data);
				next();
			}
			if(response!=null && response.status!="userFound And logged"){//Validates if the user is using a valid token
				var data = {
					status: "Error Not Valid User"
				};
				res.send(data);
				next();
			}else{
				console.log("req.params");
				console.log(req.params);
				models.Product.findById(req.query['productid'])
					.then(function(product) {
						if(product!=null){
							getIDforUsername(req,res,next);
						}else{
							var data = {
								status: "errorProductNotFound",
								data : product
							};
							res.send(data);
						}
						next();
					});

			}
		}
	}

	function getIDforUsername(req,res,next){ //Function that receives the request, response, and a next funtion
		models.Users.findOne({
			  where: {
			    user_username: req.header('username')
			  }
			})//Loads de product models and queries by id
			.then(function(user) {//Then executes coder after the query completion
				if(user!=null){
					buyProductbyID(req,res,next,user.id);
				}else{
					var data = {
						status: "errorUserNotFound",
						data : product
					};
					res.send(data);
				}
				next();
			});
	}

	function buyProductbyID(req,res,next,userID){ //Function that receives the request, response, and a next funtion
		models.Product.findById(req.query['productid'])//Loads de product models and queries by id
			.then(function(product) {//Then executes coder after the query completion
				if(product!=null){//If this user has a like on current product:
					if(product.product_quantity-req.query['buyQuantity'] >= 0){
						models.Product.decrement('product_quantity', {by: req.query['buyQuantity'],where: { id: parseInt(req.query['productid'], 10) }}); //Decrement likes on product where id = req.params - Casting to integer
						models.Transactions.create({ product_id: req.query['productid'], user_id: userID, quantity: req.query['buyQuantity']}, { fields: [ 'product_id', 'user_id' , 'quantity' ] });
						var data = {		//var data will be the json data to be sent on the response
							status: "Purchase success",
							data : product
						};
						res.send(data);		//res.send() will send data to the client
						next();	
					}else{
						var data = {		//var data will be the json data to be sent on the response
							status: "Stock is empty for this product",
							data : product
						};
						res.send(data);		//res.send() will send data to the client
						next();	
					}
					
				}
			});
	}
	
	function checkUser(req,res,next){
		var response =userAuth.checkUser(models,req,res,next);
		console.log(response);
		if(response && response.status=="userFound And logged"){//Promises async
			console.log(response.status);
			res.send(response);
			next();
		}		
	}

	function loginUser(req,res,next){
		var response =userAuth.loginUser(models,req,res,next);
		console.log(response);
		if(response && response.status=="userFound And logged"){//Promises async
			console.log(response.status);
			res.send(response);
			next();
		}		
	}
};