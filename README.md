Rest Api to manage a snacks store using node.js
	Contributor: rgarcia7
# Overview
Built on top of docker using
- Node.js(8.9.4)
- Postgresql(9.6)
- Restify(4.0.4)
- Sequelize(4.41.2)
- macaroons.js(0.3.8)

This REST API is protected by  some restrictions for example  if you do not log in, you are accessing anonymously. Furthermore, if you log in and do not have permission to do something , you will not be able to alter products for example either.

In most cases, the first step in using the REST API is to authenticate a user account. 

This is how cookie-based authentication works at a high level:

    The client creates a new session for the user, via the REST API .
    This returns a session object, which has information about the session including the session cookie. The client stores this session object.
    The client can now set the cookie in the header for all subsequent requests to the REST API.

# Authentication
This project uses macaroons which are flexible
authorization tokens that work great in distributed systems.  Like cookies,
macaroons are bearer tokens that enable applications to ascertain whether their
holders' actions are authorized.
https://ai.google/research/pubs/pub41892
-
Go to Wiki for install information and usage:
- Wiki: https://gitlab.com/rgarcia7/storeapicodechallenge/wikis/home