FROM node:8.9.4

RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY package.json .
RUN apt-get update
RUN apt-get install -y \
		make \
        python \
        postgresql-client
RUN npm install

COPY . .
WORKDIR /usr/app/src

EXPOSE 8080
ENTRYPOINT ["/bin/sh", "/usr/app/startdev.sh"]