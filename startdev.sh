#!/bin/sh
# wait-for-postgres.sh

set -e

host="postgres"

until PGPASSWORD=$POSTGRES_PASSWORD psql -h "$host" -U $POSTGRES_USER -d $POSTGRES_DB -c '\q'; do
  echo "Postgres is unavailable - sleeping"
  sleep 1
done

echo "Postgres is up - executing commands"
echo "Starting..."
npm install
../node_modules/.bin/sequelize db:migrate
../node_modules/.bin/sequelize db:seed:all
echo "Server up and Running!"
node server.js
echo "Error this line should not be seen..."
